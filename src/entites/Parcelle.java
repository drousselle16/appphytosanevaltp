package entites;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Parcelle {

    private String           idParc;
    private Date             dateSemis;
    private Date             dateRecoltePrevue;
    private Float            surface; // exprimée en hectares
    
    private EspeceCultivee   lEspeceCultivee;
    private Exploitation     lExploitation;
    private List<Traitement> lesTraitements;

    // poidsProduitUtilise
    // calcule et retourne le poids total du produit dont l'identifiant pIdProd est passé en paramètre
    // utilisé dans les traitements ( sur semence ou en champ) concernant la parcelle
    
    public Float             poidsProduitUtilise(String pIdProd){
    
    
     return null;
    
    }
     

    public Parcelle() {
    
       this.lesTraitements= new LinkedList<Traitement>();
    }

    public Parcelle(String idParc, Date dateSemis, Date dateRecoltePrevue, Float surface, EspeceCultivee lEspeceCultivee, Exploitation lExploitation) {
        
        this();
        this.idParc            = idParc;
        this.dateSemis         = dateSemis;
        this.dateRecoltePrevue = dateRecoltePrevue;
        this.surface           = surface;
        this.lEspeceCultivee   = lEspeceCultivee;
        this.lExploitation     = lExploitation;
    }
     
    //<editor-fold defaultstate="collapsed" desc="Gets & Sets">
    
    public EspeceCultivee getlEspeceCultivee() {
        return lEspeceCultivee;
    }
    
    public void setlEspeceCultivee(EspeceCultivee lEspeceCultivee) {
        this.lEspeceCultivee = lEspeceCultivee;
    }
    
    public Exploitation getlExploitation() {
        return lExploitation;
    }
    
    public void setlExploitation(Exploitation lExploitation) {
        this.lExploitation = lExploitation;
    }
    
    public List<Traitement> getLesTraitements() {
        return lesTraitements;
    }
    
    public void setLesTraitements(List<Traitement> lesTraitements) {
        this.setLesTraitements(lesTraitements);
    }
    
     public String getIdParc() {
        return idParc;
    }

    public void setIdParc(String idParc) {
        this.idParc = idParc;
    }

    public Date getDateSemis() {
        return dateSemis;
    }

    public void setDateSemis(Date dateSemis) {
        this.dateSemis = dateSemis;
    }

    public Date getDateRecoltePrevue() {
        return dateRecoltePrevue;
    }

    public void setDateRecoltePrevue(Date dateRecoltePrevue) {
        this.dateRecoltePrevue = dateRecoltePrevue;
    }

    public Float getSurface() {
        return surface;
    }

    public void setSurface(Float surface) {
        this.surface = surface;
    }

    //</editor-fold>   

    public Object getEspeceCultivee() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}